## Artivatic Task


An application in python with Django which will send email to a particular user using basic UI. This portal should be like email service and should include
email Id/ids, subject, and body, cc, bcc. Also, there should be provision to upload CSV file with a list of email Ids to send an email. Use the ELK stack for log management.

## Installation

Pre-requisites -
  - Python 3.7
  - virtualenv (Python package)

To run the server, follow the following steps -

 - Create Virtual Environment & activate
 - Clone the repository
 - Install the dependencies
 - Run the django app

### Create Virtual environment

Make sure Python 3.7 is installed. 

```bash
python -m venv email-service-venv
```
To activate the virtual environment, execute the `activate.bat` file in `email-service-venv/Scripts/`

Run,

```bash
email-service-venv/Scripts/activate
```
This would activate the virtual environment.

To clone the repository,
```bash
git clone https://poojakanungo@bitbucket.org/poojakanungo/artivatic_email_service.git
```
Note - Currently, code changes are pushed to `dev` branch. Pull accordingly. 

### Install dependencies

Once the virtual env is activated, install the required dependencies from
```bash
pip install -r requirements.txt
```
## For yagmail:
```
cd artivatic_email_service
python
import yagmail
yagmail.register("<username@gmail.com>","<password>")
```
### Usage

To run the server, make sure you are in the `ml_platform` directory and execute

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```
